<?php
/**
 * Created by PhpStorm.
 * User: nhiha60591
 * Date: 21/12/2016
 * Time: 21:31
 */
if( !function_exists( 'trans_value' ) ){
    function trans_value($key=NULL, $language_id=0){
        $trans = \App\Models\TextTrans::where('key', $key);
        if( $trans->count() ){
            if( $language_id == 0 ){
                $currentLanguage = \App\Models\Languages::where('code', app()->getLocale() )->first();
                if( $trans->where('language_id', $currentLanguage->id)->count() ){
                    return $trans->where('language_id', $currentLanguage->id)->first()->value;
                }else{
                    return $trans->first()->value;
                }
            }
            if( $trans->where('language_id', $language_id)->count() ){
                return $trans->where('language_id', $language_id)->first()->value;
            }else{
                return $trans->first()->value;
            }
        }else{
            return $key;
        }
    }
}