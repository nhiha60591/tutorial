<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Front\FrontController;
use App\Models\Article;

class HomeController extends FrontController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getIndex()
    {
        $articles = Article::all();
        $view = ['articles' => $articles];
        return $this->theme->scope('home.index', $view)->render();
    }
    public function getFeature()
    {
        $view = [];
        return $this->theme->scope('home.feature', $view)->render();
    }
    public function getPortfolio()
    {
        $view = [];
        return $this->theme->scope('home.portfolio', $view)->render();
    }
    public function getBlog()
    {
        $view = [];
        return $this->theme->scope('home.blog', $view)->render();
    }
    public function getContact()
    {
        $view = [];
        return $this->theme->scope('home.contact', $view)->render();
    }
}
