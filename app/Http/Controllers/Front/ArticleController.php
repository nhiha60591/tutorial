<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Front\FrontController;

class ArticleController extends FrontController
{
    protected $pageTitle = 'Articles';
    public function __construct()
    {
        parent::__construct();
    }
    public function getIndex()
    {
        $view = [];
        return $this->theme->scope('article.index', $view)->render();
    }
}
