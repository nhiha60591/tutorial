<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Theme;

class FrontController extends BaseController
{
    /**
     * Theme instance.
     *
     * @var \Teepluss\Theme\Theme
     */
    protected $theme;
    protected $pageTitle = 'Home Page';
    protected $siteTitle = 'Huu Hien Multiple Languages';

    /**
     * Construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->setTheme();
    }

    /**
     * Setup for theme
     */
    public function setTheme(){
        // Using theme as a global.
        $this->theme = Theme::uses('default')->layout('default');
        $this->theme->setTitle($this->pageTitle. " - ". $this->siteTitle);
    }
}
