<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Languages;
use Illuminate\Support\Facades\Storage;
use Validator;

class LanguageController extends AdminController
{
    protected $menuActive = 'language';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $languages = Languages::paginate(20);
        $this->setTheme();
        $view = array(
            'languages' => $languages
        );
        return $this->theme->scope('language.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->pageTitle = 'Create new language';
        $this->setTheme();
        $this->theme->asset()->usePath()->add('bootstrap-select', 'plugins/bootstrap-select/css/bootstrap-select.css', ['bootstrap']);
        return $this->theme->scope('language.create')->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|max:255',
            'name' => 'required|max:255',
            'locale' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect(route('languages.create'))
                ->withErrors($validator)
                ->withInput();
        }
        $language = new Languages();
        $language->code = $request->input('code');
        $language->name = $request->input('name');
        $language->locale = $request->input('locale');
        if( $request->hasFile('image') ){
            $path = $request->file('image')->store('languages');
            $language->image = $path;
        }
        $language->save();
        return redirect(route('languages.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        die('asdasdas');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->pageTitle = 'Edit language';
        $this->setTheme();
        $view = [];
        $view['language'] = Languages::find($id);
        $this->theme->asset()->usePath()->add('bootstrap-select', 'plugins/bootstrap-select/css/bootstrap-select.css', ['bootstrap']);
        return $this->theme->scope('language.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'code' => 'required|max:255',
            'name' => 'required|max:255',
            'locale' => 'required|max:255',
        ]);
        if ($validator->fails()) {
            return redirect(route('languages.edit', $id))
                ->withErrors($validator)
                ->withInput();
        }
        $language = Languages::find($id);
        $language->code = $request->input('code');
        $language->name = $request->input('name');
        $language->locale = $request->input('locale');
        if( $request->hasFile('image') ){
            $path = $request->file('image')->store('languages');
            $language->image = $path;
        }
        $language->save();
        return redirect(route('languages.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $return = [
            'status' => 0,
            'message' => 'Have some problem while remove the language'
        ];
        try{
            Languages::destroy($id);
            $return = [
                'status' => 1,
                'message' => 'Remove success!'
            ];
        }catch (\Exception $exception ){
            $exception->getMessage();
        }

        return json_encode( $return );
    }

    public function getTextTrans(){
        return $this->theme->scope('language.get-text-trans')->render();
    }
}
