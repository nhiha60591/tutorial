<?php

namespace App\Http\Controllers\Admin;

use App\Models\Languages;
use App\Models\TextTrans;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TextTransController extends AdminController
{
    protected $menuActive = 'language';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $currentLanguage = Languages::where('code', app()->getLocale())->first();
        $textTrans = TextTrans::where('language_id', $currentLanguage->id)->paginate(20);
        $view = [
            'currentLanguage' => $currentLanguage,
            'textTrans' => $textTrans
        ];
        return $this->theme->scope('text-trans.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
