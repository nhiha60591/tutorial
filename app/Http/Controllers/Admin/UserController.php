<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends AdminController
{
    protected $menuActive = 'user';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($data = NULL)
    {
        $view = [];
        $users = User::paginate(20);
        $view['users'] = $users;
        $this->pageTitle = 'List all users';
        $this->setTheme();
        return $this->theme->scope('user.index', $view)->render();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->pageTitle = 'Create new user';
        $this->setTheme();
        $this->theme->asset()->usePath()->add('bootstrap-select', 'plugins/bootstrap-select/css/bootstrap-select.css', ['bootstrap']);
        return $this->theme->scope('user.create')->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $this->pageTitle = 'Show user';
        $this->setTheme();
        return $this->theme->scope('user.detail')->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->pageTitle = 'Edit user';
        $this->setTheme();
        $view = [];
        $view['user'] = User::find($id);
        $this->theme->asset()->usePath()->add('bootstrap-select', 'plugins/bootstrap-select/css/bootstrap-select.css', ['bootstrap']);
        return $this->theme->scope('user.edit', $view)->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
