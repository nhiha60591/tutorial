<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

class DashboardController extends AdminController
{
    public function __construct()
    {
        parent::__construct();
    }
    public function getIndex($data = NULL){
    	return $this->theme->scope('dashboard.dashboard')->render();
    }
}
