<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseController;
use Theme;

class AdminController extends BaseController
{
    /**
     * Theme instance.
     *
     * @var \Teepluss\Theme\Theme
     */
    protected $theme;
    protected $pageTitle = 'Dashboard';
    protected $siteTitle = 'Huu Hien Multiple Languages';
    protected $subTitle = 'Dashboard page';
    protected $menuActive = 'home';
    protected $columns = ['id'=>'ID', 'actions' => 'Actions'];
    protected $fields = ['name' => ['type' => 'text', 'label' => 'Name']];
    protected $_data = NULL;

    /**
     * Construct
     *
     * @return void
     */
    public function __construct()
    {
        $this->setTheme();
    }

    public function setTheme(){
        // Using theme as a global.
        $this->theme = Theme::uses('admin')->layout('default');
        $this->theme->setTitle($this->pageTitle. " - ". $this->siteTitle);
        $this->theme->setPagetitle($this->pageTitle);
        $this->theme->setSubtitle($this->subTitle);
        $this->theme->setMenuactive($this->menuActive);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->getIndex( $this->_data );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

    }

    /**
     * Get list data page
     * @author Hien(Hamilton) H.HO
     * @date 17 Dec 2016
     */
    public function getIndex($data = NULL)
    {
        $view['data'] = $data;
        $view['columns'] = $this->columns;
        $view['fields'] = $this->fields;
        $this->setTheme();
        return $this->theme->scope('admin.index', $view)->render();
    }

    /**
     * Get edit data page
     * @author Hien(Hamilton) H.HO
     * @date 18 Dec 2016
     */
    public function getEdit($data = NULL)
    {
        $view['data'] = $data;
        $view['columns'] = $this->columns;
        $view['fields'] = $this->fields;
        $this->setTheme();
        return $this->theme->scope('admin.edit', $view)->render();
    }

    /**
     * Get edit data page
     * @author Hien(Hamilton) H.HO
     * @date 18 Dec 2016
     */
    public function getCreate($data = NULL)
    {
        $view['data'] = $data;
        $view['columns'] = $this->columns;
        $view['fields'] = $this->fields;
        $this->setTheme();
        return $this->theme->scope('admin.create', $view)->render();
    }

    /**
     * Get edit data page
     * @author Hien(Hamilton) H.HO
     * @date 18 Dec 2016
     */
    public function getDetail($data=NULL){
        $view['data'] = $data;
        $view['columns'] = $this->columns;
        $view['fields'] = $this->fields;
        $this->setTheme();
        return $this->theme->scope('admin.detail', $view)->render();
    }
}
