<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
	protected $table = 'articles';
    /**
     * Many-to-Many relations with User.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function trans() {
        return $this->hasMany('App\Models\ArticleTrans');
    }

    /**
     * Translate role 
     * 
     * @param  integer $language_id [description]
     * @param  string  $field       [description]
     * @return [type]               [description]
     */
    public function translation($language_code=1, $field='name'){
        $language_id = Languages::where('code', $language_code)->first()->id;
        if( $this->trans()->where('language_id', $language_id)->count() ) {
            $trans = $this->trans()->where('language_id', $language_id)->first()->$field;
            return $trans;
        }else{
            return null;
        }
    }
}
