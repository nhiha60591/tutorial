<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TextTrans extends Model
{
    protected $table = 'text_trans';
    protected $fillable = ['key', 'value'];
}
