<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArticleTrans extends Model
{
    protected $table = 'article_trans';

    public function article(){
        return $this->belongsToMany('App\Models\Article', 'article_id');
    }
    public function language(){
        return $this->belongsToMany('App\Models\Languages', 'language_id');
    }
}
