<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Languages extends Model
{
    protected $table = 'languages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'name', 'locale', 'image', 'status', 'position'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'language_id'
    ];

    /**
     * Get all language codes
     *
     * @return array
     */
    public static function getAllLanguageCodes(){
        $languages = static::where('status', 'active')->get();
        $codes = [];
        foreach ($languages as $lang){
            $codes[] = $lang->code;
        }
        return $codes;
    }

    public static function getDefaultLanguage(){
        return static::where('default', 1)->first();
    }
}
