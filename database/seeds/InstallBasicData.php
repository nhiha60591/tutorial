<?php

use Illuminate\Database\Seeder;

class InstallBasicData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /**
         * Insert Default Languages
         */
        $enID = DB::table('languages')->insertGetId([
            'code' => 'en',
            'name' => 'English',
            'locale' => 'EN_US',
            'status' => 'active',
            'default' => 1,
            'position' => 1,
            'image' => 'languages/en.png'
        ]);
        $viID = DB::table('languages')->insertGetId([
            'code' => 'vi',
            'name' => 'Vietnamese',
            'locale' => 'vi',
            'status' => 'active',
            'position' => 2,
            'image' => 'languages/vi.png'
        ]);

        /**
         * Insert admin user
         */
        DB::table('users')->insert([
            'name' => 'Administrator',
            'email' => 'admin@gmail.com',
            'is_admin' => 1,
            'password' => bcrypt('123123123')
        ]);

        /**
         * Insert some default settings
         */
        DB::table('settings')->insert([
            'key' => 'site_name',
            'value' => 'Laravel Multiple Languages',
            'language_id' => $enID
        ]);
        DB::table('settings')->insert([
            'key' => 'keyword',
            'value' => 'Laravel Multiple Languages',
            'language_id' => $enID
        ]);
        DB::table('settings')->insert([
            'key' => 'description',
            'value' => 'Laravel Multiple Languages',
            'language_id' => $enID
        ]);
        DB::table('settings')->insert([
            'key' => 'author',
            'value' => 'Hien(Hamilton) H.HO',
            'language_id' => $enID
        ]);
    }
}
