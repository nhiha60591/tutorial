<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
try {
	$allLanguageCodes = \App\Models\Languages::getAllLanguageCodes();
	$defaultLanguage = \App\Models\Languages::getDefaultLanguage();
	if ($defaultLanguage) {
		$defaultLanguageCode = $defaultLanguage->code;
	} else {
		$defaultLanguageCode = 'en';
	}
	$currentLanguageCode = Request::segment(1, $defaultLanguageCode);

	if (in_array($currentLanguageCode, $allLanguageCodes) ) {
		Route::get('/', function () use ($defaultLanguageCode) {
			return redirect()->to($defaultLanguageCode);
		});

		Route::group(['namespace' => 'Front', 'prefix' => $currentLanguageCode], function () use($currentLanguageCode) {
			/*Set locale*/
			app()->setLocale($currentLanguageCode);

			Route::get('/', 'HomeController@getIndex')->name('front.home');
			Route::get('/features', 'HomeController@getFeature')->name('front.features');
			Route::get('/portfolio', 'HomeController@getPortfolio')->name('front.portfolio');
			Route::get('/blog', 'HomeController@getBlog')->name('front.blog');
			Route::get('/contact', 'HomeController@getContact')->name('front.contact');
		});
        Route::group(['prefix' => $currentLanguageCode], function () use($currentLanguageCode) {
            /*Set locale*/
            app()->setLocale($currentLanguageCode);
            adminRoute();
        });
	}else{
		Route::get('/'.$currentLanguageCode, function () use ($defaultLanguageCode) {
			return redirect()->to($defaultLanguageCode);
		});
	}
} catch (\Exception $e) {
}
function adminRoute(){
    Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware'=>'auth.admin', 'as' => 'admin.'], function () {
        Route::get('/', ['uses' => 'DashboardController@getIndex', 'as' => 'dashboard']);
        Route::group(['prefix' => 'languages', 'as'=>'languages.'], function (){
            Route::resource('/', 'LanguageController');
            Route::resource('/text', 'TextTransController');
        });
        Route::resource('/users', 'UserController');
        Route::resource('/articles', 'ArticleController');
        Route::resource('/categories', 'CategoryController');
        Route::resource('/tags', 'TagController');
        Route::resource('/roles', 'RoleController');
        Route::resource('/permissions', 'PermissionController');
    });
}
adminRoute();
Auth::routes();
