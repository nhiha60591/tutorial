<?php
return [
    'welcome' => 'Welcome to this tutorial!',
    'home' => 'Home',
    'features' => 'Feature',
    'portfolio' => 'Portfolio',
    'blog' => 'Blog',
    'contact' => 'Contact',
];