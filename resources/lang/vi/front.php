<?php
return [
    'welcome' => 'Chào mừng bạn đến với tutorial này!',
    'home' => 'Trang chủ',
    'features' => 'Nổi bật',
    'portfolio' => 'Trang cá nhân',
    'blog' => 'Tin tức',
    'contact' => 'Liên hệ',
];