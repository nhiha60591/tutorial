<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
            $theme->setTitle('Copyright ©  2013 - Laravel.in.th');

            // Breadcrumb template.
            // $theme->breadcrumb()->setTemplate('
            //     <ul class="breadcrumb">
            //     @foreach ($crumbs as $i => $crumb)
            //         @if ($i != (count($crumbs) - 1))
            //         <li><a href="{{ $crumb["url"] }}">{!! $crumb["label"] !!}</a><span class="divider">/</span></li>
            //         @else
            //         <li class="active">{!! $crumb["label"] !!}</li>
            //         @endif
            //     @endforeach
            //     </ul>
            // ');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // You may use this event to set up your assets.
            $theme->asset()->usePath()->add('bootstrap', 'plugins/bootstrap/css/bootstrap.css');
            $theme->asset()->usePath()->add('waves', 'plugins/node-waves/waves.css');
            $theme->asset()->usePath()->add('animate', 'plugins/animate-css/animate.css');
            $theme->asset()->usePath()->add('morris', 'plugins/morrisjs/morris.css');
            $theme->asset()->usePath()->add('style', 'css/style.css');
            $theme->asset()->usePath()->add('all-themes', 'css/themes/all-themes.css');

            $theme->asset()->container('footer')->usePath()->add('jquery', 'plugins/jquery/jquery.min.js');
            $theme->asset()->container('footer')->usePath()->add('cpanel-ajax', 'js/ajax.js');
            $theme->asset()->container('footer')->usePath()->add('bootstrap', 'plugins/bootstrap/js/bootstrap.js');
            $theme->asset()->container('footer')->usePath()->add('bootstrap-select', 'plugins/bootstrap-select/js/bootstrap-select.js');
            $theme->asset()->container('footer')->usePath()->add('jquery.slimscroll', 'plugins/jquery-slimscroll/jquery.slimscroll.js');
            $theme->asset()->container('footer')->usePath()->add('node-waves', 'plugins/node-waves/waves.js');
            $theme->asset()->container('footer')->usePath()->add('countTo', 'plugins/jquery-countto/jquery.countTo.js');
            $theme->asset()->container('footer')->usePath()->add('raphael', 'plugins/raphael/raphael.min.js');
            $theme->asset()->container('footer')->usePath()->add('morris', 'plugins/morrisjs/morris.js');
            $theme->asset()->container('footer')->usePath()->add('Chart.bundle', 'plugins/chartjs/Chart.bundle.js');
            $theme->asset()->container('footer')->usePath()->add('jquery.flot', 'plugins/flot-charts/jquery.flot.js');
            $theme->asset()->container('footer')->usePath()->add('jquery.resize', 'plugins/flot-charts/jquery.flot.resize.js');
            $theme->asset()->container('footer')->usePath()->add('jquery.pie', 'plugins/flot-charts/jquery.flot.pie.js');
            $theme->asset()->container('footer')->usePath()->add('jquery.categories', 'plugins/flot-charts/jquery.flot.categories.js');
            $theme->asset()->container('footer')->usePath()->add('jquery.time', 'plugins/flot-charts/jquery.flot.time.js');
            $theme->asset()->container('footer')->usePath()->add('sparkline', 'plugins/jquery-sparkline/jquery.sparkline.js');
            $theme->asset()->container('footer')->usePath()->add('admin', 'js/admin.js');
            $theme->asset()->container('footer')->usePath()->add('index', 'js/pages/index.js');
            $theme->asset()->container('footer')->usePath()->add('demo', 'js/demo.js');
            // $theme->asset()->add('jquery', 'vendor/jquery/jquery.min.js');
            // $theme->asset()->add('jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', array('jquery'));

            // Partial composer.
            // $theme->partialComposer('header', function($view)
            // {
            //     $view->with('auth', Auth::user());
            // });
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(

            'default' => function($theme)
            {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);