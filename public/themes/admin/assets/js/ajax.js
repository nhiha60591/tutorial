jQuery(document).ready(function($){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $('body').on('click', '.btn-remove-item', function () {
        var ID = $(this).attr('data-id');
        var r = confirm("Are you sure to remove this item!");
        if (r == true) {
            $.ajax({
                type: $(this).attr('data-method'),
                url: $(this).attr('href'),
                success: function (data) {
                    var response = JSON.parse(data);
                    alert( response.message );
                    $(ID).remove();
                },
                error: function (data) {
                    var response = JSON.parse(data);
                    alert( response.message );
                }
            });
        }
        return false;
    });
});