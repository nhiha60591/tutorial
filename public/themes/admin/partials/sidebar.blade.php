<!-- #Top Bar -->
<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img src="{{ Auth::user()->avatar ? url(Auth::user()->avatar) : Theme::asset()->url('images/user.png') }}" width="48" height="48" alt="User" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{ Auth::user()->name }}</div>
                <div class="email">{{ Auth::user()->email }}</div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right">
                        <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">group</i>Followers</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">shopping_cart</i>Sales</a></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">favorite</i>Likes</a></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="javascript:void(0);"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="header">MAIN NAVIGATION</li>
                <li @if( Theme::get('menuactive') == 'home' ) class="active" @endif >
                    <a href="{{ route('admin.dashboard') }}">
                        <i class="material-icons">home</i>
                        <span>{{ trans_value('home') }}</span>
                    </a>
                </li>
                <li @if( Theme::get('menuactive') == 'language' ) class="active" @endif>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">language</i>
                        <span>Language</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('admin.languages.index') }}">
                                <span>Language</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.languages.create') }}">
                                <span>Add new language</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.languages.text.index') }}">
                                <span>Text trans</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li @if( Theme::get('menuactive') == 'article' ) class="active" @endif>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">library_books</i>
                        <span>Articles</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('admin.articles.index') }}">
                                <span>List article</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.articles.create') }}">
                                <span>Add new article</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.categories.index') }}">
                                <span>Categories</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.tags.index') }}">
                                <span>Add new article</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li @if( Theme::get('menuactive') == 'user' ) class="active" @endif>
                    <a href="javascript:void(0);" class="menu-toggle">
                        <i class="material-icons">supervisor_account</i>
                        <span>Users</span>
                    </a>
                    <ul class="ml-menu">
                        <li>
                            <a href="{{ route('admin.users.index') }}">
                                <span>List users</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.users.create') }}">
                                <span>Add new user</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.roles.index') }}">
                                <span>Roles</span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('admin.permissions.index') }}">
                                <span>Permissions</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                &copy; 2016 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
            </div>
            <div class="version">
                <b>Version: </b> 1.0.4
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>