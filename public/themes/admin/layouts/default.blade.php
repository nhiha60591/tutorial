<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>{!! Theme::get('title') !!}</title>
    <meta charset="utf-8">
    <meta name="keywords" content="{!! Theme::get('keywords') !!}">
    <meta name="description" content="{!! Theme::get('description') !!}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
    {!! Theme::asset()->styles() !!}
    {!! Theme::asset()->scripts() !!}
</head>

<body class="theme-red">
{!! Theme::partial('header') !!}
{!! Theme::partial('sidebar') !!}


<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <h2>{!! Theme::get('pagetitle') !!}</h2>
        </div>
        {!! Theme::partial('messages') !!}
        {!! Theme::content() !!}
    </div>
</section>

{!! Theme::partial('footer') !!}

{!! Theme::asset()->container('footer')->scripts() !!}

</body>

</html>