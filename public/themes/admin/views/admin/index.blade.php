<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    {!! Theme::get('subtitle') !!}
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        @foreach($columns as $k=>$v)
                            <th>{{ $v }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                        @if( isset($data) && $data->count() > 0 )
                            @foreach( $data as $item )
                                <tr id="language-item-{{ $item->id }}">
                                    @foreach($columns as $k=>$v)
                                        <th>{{ $item->$k }}</th>
                                    @endforeach
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="6">{{ trans('admin.item-not-found') }}</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
                {{ $data->links() }}
            </div>
        </div>
    </div>
</div>