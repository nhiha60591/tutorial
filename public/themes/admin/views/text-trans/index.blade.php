<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    LIST ALL TEXT TRANSLATIONS
                    <small>LIST ALL TEXT TRANSLATIONS</small>
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>{{ trans_value('key') }}</th>
                        <th>{{ trans_value('value') }}</th>
                        <th>{{ trans_value('actions') }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if( isset($textTrans) && $textTrans->count() > 0 )
                        @foreach( $textTrans as $text )
                            <tr id="text-item-{{ $text->id }}">
                                <th scope="row">{{ $text->id }}</th>
                                <td>{{ $text->key }}</td>
                                <td>{{ $text->value }}</td>
                                <td>
                                    <a href="{{ route('admin.languages.text.edit', $text->id) }}" class="btn btn-default waves-effect">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a href="{{ route('admin.languages.text.destroy', $text->id) }}" class="btn btn-default waves-effect btn-remove-item" data-method="DELETE" data-id="#text-item-{{ $text->id }}">
                                        <i class="material-icons">remove</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">Text not found!</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                {{ $textTrans->links() }}
            </div>
        </div>
    </div>
</div>