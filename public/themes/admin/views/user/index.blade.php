<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    LIST ALL USERS
                    <small>List all current users</small>
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>AVATAR</th>
                        <th>NAME</th>
                        <th>EMAIL</th>
                        <th>ACTIONS</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if( isset($users) && $users->count() > 0 )
                        @foreach( $users as $user )
                            <tr id="user-item-{{ $user->id }}">
                                <th scope="row">{{ $user->id }}</th>
                                <td><img src="{{ asset('uploads/'.$user->avatar) }}" width="50" /></td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    {{--<a href="{{ route('languages.show', $language->id) }}" class="btn btn-default waves-effect">
                                        <i class="material-icons">remove_red_eye</i>
                                    </a>--}}
                                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-default waves-effect">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a href="{{ route('users.destroy', $user->id) }}" class="btn btn-default waves-effect btn-remove-item" data-method="DELETE" data-id="#user-item-{{ $user->id }}">
                                        <i class="material-icons">remove</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">Language not found!</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                {{ $users->links() }}
            </div>
        </div>
    </div>
</div>