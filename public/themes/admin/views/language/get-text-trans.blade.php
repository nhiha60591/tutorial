<div class="row clearfix">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="card">
            <div class="header">
                <h2>
                    ALL STRING TEXT
                    <small>List all current string language text</small>
                </h2>
            </div>
            <div class="body table-responsive">
                <table class="table">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>IMAGE</th>
                        <th>CODE</th>
                        <th>NAME</th>
                        <th>ACTIONS</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if( isset($textTrans) && $textTrans->count() > 0 )
                        @foreach( $languages as $language )
                            <tr id="language-item-{{ $language->id }}">
                                <th scope="row">{{ $language->id }}</th>
                                <td><img src="{{ asset('uploads/'.$language->image) }}" width="50" /></td>
                                <td>{{ $language->code }}</td>
                                <td>{{ $language->name }}</td>
                                <td>
                                    {{--<a href="{{ route('languages.show', $language->id) }}" class="btn btn-default waves-effect">
                                        <i class="material-icons">remove_red_eye</i>
                                    </a>--}}
                                    <a href="{{ route('languages.edit', $language->id) }}" class="btn btn-default waves-effect">
                                        <i class="material-icons">edit</i>
                                    </a>
                                    <a href="{{ route('languages.destroy', $language->id) }}" class="btn btn-default waves-effect btn-remove-item" data-method="DELETE" data-id="#language-item-{{ $language->id }}">
                                        <i class="material-icons">remove</i>
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="6">Language not found!</td>
                        </tr>
                    @endif
                    </tbody>
                </table>
                {{ $languages->links() }}
            </div>
        </div>
    </div>
</div>