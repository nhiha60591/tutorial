<!-- Input -->
<div class="row clearfix">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="card">
      <div class="body">
        {!! Form::open(['route' => 'admin.languages.store', 'files' => true]) !!}
        <div class="form-group">
          {!! Form::label('code', 'Language code') !!}
          <div class="form-line">
            {!! Form::text('code', old('code'), ['class'=>'form-control']) !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('locale', 'Language locale') !!}
          <div class="form-line">
            {!! Form::text('locale', old('locale'), ['class'=>'form-control']) !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('name', 'Language name') !!}
          <div class="form-line">
            {!! Form::text('name', old('name'), ['class'=>'form-control']) !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('status', 'Language status') !!}
          <div class="form-line">
            {!! Form::select('status', ['active' => trans('admin.active'), 'deactive' => trans('admin.deactive')], old('status'), ['class'=>'form-control']) !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::label('image', 'Language image') !!}
          <div class="form-line">
            {!! Form::file('image', ['class'=>'form-control']) !!}
          </div>
        </div>
        <div class="form-group">
          {!! Form::submit('Save change', ['class'=>'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>
</div>