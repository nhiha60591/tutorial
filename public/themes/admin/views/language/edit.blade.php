    <!-- Input -->
    <div class="row clearfix">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <div class="body">
                    {!! Form::open(['route' => ['admin.languages.update', $language->id], 'method' => 'put', 'files' => true ]) !!}
                        <div class="form-group">
                            {!! Form::label('code', 'Language code') !!}
                            <div class="form-line">
                                {!! Form::text('code', $language->code, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('locale', 'Language locale') !!}
                            <div class="form-line">
                                {!! Form::text('locale', $language->locale, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('name', 'Language name') !!}
                            <div class="form-line">
                                {!! Form::text('name', $language->name, ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::label('status', 'Language status') !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('status', 'Status') !!}
                            {!! Form::select('status', ['active' => trans('admin.active'), 'deactive' => trans('admin.deactive')], $language->status, ['class'=>'form-control show-tick']) !!}
                        </div>
                        <div class="form-group">
                            {!! Form::label('image', 'Language image') !!}
                            <div class="form-line">
                                {!! Form::file('image', ['class'=>'form-control']) !!}
                            </div>
                        </div>
                        <div class="form-group">
                            {!! Form::submit('Save change', ['class'=>'btn btn-primary']) !!}
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>