<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Inherit from another theme
    |--------------------------------------------------------------------------
    |
    | Set up inherit from another if the file is not exists,
    | this is work with "layouts", "partials", "views" and "widgets"
    |
    | [Notice] assets cannot inherit.
    |
    */

    'inherit' => null, //default

    /*
    |--------------------------------------------------------------------------
    | Listener from events
    |--------------------------------------------------------------------------
    |
    | You can hook a theme when event fired on activities
    | this is cool feature to set up a title, meta, default styles and scripts.
    |
    | [Notice] these event can be override by package config.
    |
    */

    'events' => array(

        // Before event inherit from package config and the theme that call before,
        // you can use this event to set meta, breadcrumb template or anything
        // you want inheriting.
        'before' => function($theme)
        {
            // You can remove this line anytime.
            $theme->setTitle('Copyright ©  2013 - Laravel.in.th');

            // Breadcrumb template.
            // $theme->breadcrumb()->setTemplate('
            //     <ul class="breadcrumb">
            //     @foreach ($crumbs as $i => $crumb)
            //         @if ($i != (count($crumbs) - 1))
            //         <li><a href="{{ $crumb["url"] }}">{!! $crumb["label"] !!}</a><span class="divider">/</span></li>
            //         @else
            //         <li class="active">{!! $crumb["label"] !!}</li>
            //         @endif
            //     @endforeach
            //     </ul>
            // ');
        },

        // Listen on event before render a theme,
        // this event should call to assign some assets,
        // breadcrumb template.
        'beforeRenderTheme' => function($theme)
        {
            // You may use this event to set up your assets.
            $theme->asset()->usePath()->add('bootstrap', 'css/bootstrap.min.css');
            $theme->asset()->usePath()->add('flexslider', 'plugins/flexslider/flexslider.css');
            $theme->asset()->usePath()->add('cubeportfolio', 'css/cubeportfolio.min.css');
            $theme->asset()->usePath()->add('style', 'css/style.css');
            $theme->asset()->usePath()->add('skins-default', 'skins/default.css');
            $theme->asset()->usePath()->add('bodybg-bg1', 'bodybg/bg1.css');



            $theme->asset()->container('footer')->usePath()->add('jquery', 'js/jquery.min.js');
            $theme->asset()->container('footer')->usePath()->add('modernizr', 'js/modernizr.custom.js');
            $theme->asset()->container('footer')->usePath()->add('jquery.easing', 'js/jquery.easing.1.3.js');
            $theme->asset()->container('footer')->usePath()->add('bootstrap', 'js/bootstrap.min.js');
            $theme->asset()->container('footer')->usePath()->add('jquery.flexslider', 'plugins/flexslider/jquery.flexslider-min.js');
            $theme->asset()->container('footer')->usePath()->add('flexslider.config', 'plugins/flexslider/flexslider.config.js');
            $theme->asset()->container('footer')->usePath()->add('jquery.appear', 'js/jquery.appear.js');
            $theme->asset()->container('footer')->usePath()->add('stellar', 'js/stellar.js');
            $theme->asset()->container('footer')->usePath()->add('classie', 'js/classie.js');
            $theme->asset()->container('footer')->usePath()->add('uisearch', 'js/uisearch.js');
            $theme->asset()->container('footer')->usePath()->add('cubeportfolio', 'js/jquery.cubeportfolio.min.js');
            $theme->asset()->container('footer')->usePath()->add('google-code-prettify', 'js/google-code-prettify/prettify.js');
            $theme->asset()->container('footer')->usePath()->add('animate', 'js/animate.js');
            $theme->asset()->container('footer')->usePath()->add('custom', 'js/custom.js');
            // $theme->asset()->add('jquery', 'vendor/jquery/jquery.min.js');
            // $theme->asset()->add('jquery-ui', 'vendor/jqueryui/jquery-ui.min.js', array('jquery'));

            // Partial composer.
            // $theme->partialComposer('header', function($view)
            // {
            //     $view->with('auth', Auth::user());
            // });
        },

        // Listen on event before render a layout,
        // this should call to assign style, script for a layout.
        'beforeRenderLayout' => array(

            'default' => function($theme)
            {
                // $theme->asset()->usePath()->add('ipad', 'css/layouts/ipad.css');
            }

        )

    )

);